var fs = require("fs");
var path = require('path');
var swPrecache = require('sw-precache');
var rootDir, config, book;

module.exports = {
  website: {
    assets: "./book",
    js: [
      "service-worker-registration.js"
    ]
  },
  hooks: {
    finish: function() {
      rootDir = this.book.options.output;
      console.log(rootDir);
      swPrecache.write(rootDir + '/service-worker.js', {
        staticFileGlobs: [rootDir + '/**/*.{js,html,css,png,PNG,jpg,gif,svg,eot,ttf,woff}'],
        stripPrefix: rootDir
      }, function() {
        console.log('sw.js generated');
      });
    }
  }
};
