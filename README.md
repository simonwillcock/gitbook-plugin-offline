# Gitbook Offline Plugin

This plugin uses [sw-precache](https://github.com/GoogleChrome/sw-precache) - a module written by Google - to automatically generate a service worker and 
include it in your gitbook output. By default it will pre-cache all assets 
in your book so that they are available offline.

## Installing

Add this to your gitbook by including it in your `book.json` file.

```
{
    "plugins": [offline"]
}
```

Be sure to run `gitbook install` before building your book, or as part of your automated build process. 

## Configuration

There are currently no configuration options - just install and go!
